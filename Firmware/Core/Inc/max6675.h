//
// Created by Csurleny on 12/4/2021.
//

#ifndef FURNACECONTROL_MAX6675_H
#define FURNACECONTROL_MAX6675_H

//#define FLOAT_RESULT

#include "main.h"

#ifdef FLOAT_RESULT
float max6675_get_temp(void);
#else
uint16_t max6675_get_temp(void);
#endif

#endif //FURNACECONTROL_MAX6675_H
