//
// Created by Csurleny on 12/4/2021.
//

#ifndef FURNACECONTROL_LCD_H
#define FURNACECONTROL_LCD_H

// LCD function prototypes
void lcd_init(void);
void lcd_send_cmd (char cmd);
void lcd_send_data (char data);
void lcd_send_string (char *str);
void lcd_put_cur(int row, int col);
void lcd_clear (void);

#endif //FURNACECONTROL_LCD_H
