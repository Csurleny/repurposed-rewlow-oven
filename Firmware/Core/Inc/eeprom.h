//
// Created by Csurleny on 12/4/2021.
//

#ifndef FURNACECONTROL_EEPROM_H
#define FURNACECONTROL_EEPROM_H


#include "main.h"

typedef enum {
    EEPROM_STATUS_PENDING,
    EEPROM_STATUS_COMPLETE,
    EEPROM_STATUS_ERROR
} EepromOperations;

void EEPROM_SPI_INIT(SPI_HandleTypeDef * hspi);
EepromOperations EEPROM_SPI_WriteBuffer(uint8_t* pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite);
EepromOperations EEPROM_WritePage(uint8_t* pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite);
EepromOperations EEPROM_SPI_ReadBuffer(uint8_t* pBuffer, uint16_t ReadAddr, uint16_t NumByteToRead);
uint8_t EEPROM_SPI_WaitStandbyState(void);


#endif //FURNACECONTROL_EEPROM_H
