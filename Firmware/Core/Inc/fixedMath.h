//
// Created by Csurleny on 12/12/2021.
//

#ifndef FURNACECONTROL_FIXEDMATH_H
#define FURNACECONTROL_FIXEDMATH_H

#include <stdint.h>

typedef int32_t SFRAC32;
typedef int16_t SFRAC16;

#define Q15(Float_Value) \
        (((Float_Value) < 0.0) ? (SFRAC16)(32768 * (Float_Value) - 0.5) \
        : (SFRAC16)(32767 * (Float_Value) + 0.5))
#define Q31(Float_Value) \
        (((Float_Value) < 0.0) ? (SFRAC32)(2147483648 * (Float_Value) - 0.5) \
        : (SFRAC32)(2147483648 * (Float_Value) + 0.5))

SFRAC32 temp_to_fract(uint16_t temp);
uint16_t fract_to_temp(SFRAC32 tempF);
uint16_t fract_to_duty(SFRAC32 dutyF);


#endif //FURNACECONTROL_FIXEDMATH_H
