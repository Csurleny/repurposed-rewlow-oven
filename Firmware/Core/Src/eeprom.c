//
// Created by Csurleny on 12/4/2021.
//

#include "eeprom.h"

#define EEPROM_WREN  0x06  /* Write Enable */
#define EEPROM_WRDI  0x04  /* Write Disable */
#define EEPROM_RDSR  0x05  /* Read Status Register */
#define EEPROM_WRSR  0x01  /* Write Status Register */
#define EEPROM_READ  0x03  /* Read from Memory Array */
#define EEPROM_WRITE 0x02  /* Write to Memory Array */

#define EEPROM_WIP_FLAG        0x01  /* Write In Progress (WIP) flag */

#define EEPROM_PAGESIZE        64    /* Page-size according to documentation */
#define EEPROM_BUFFER_SIZE     32    /* EEPROM Buffer size. Setup to your needs */

#define EEPROM_CS_HIGH()    HAL_GPIO_WritePin(rCS_GPIO_Port, rCS_Pin, GPIO_PIN_SET)
#define EEPROM_CS_LOW()     HAL_GPIO_WritePin(rCS_GPIO_Port, rCS_Pin, GPIO_PIN_RESET)

static SPI_HandleTypeDef *EEPROM_SPI;
static uint8_t EEPROM_StatusByte;
static uint8_t RxBuffer[EEPROM_BUFFER_SIZE] = {0x00};

/*
 * Function: EEPROM_SPI_SendInstruction
 * ----------------------------
 *   Initiates the EEPROM with the given instruction(s)
 *
 *   instruction: the required instruction(s) (see defines)
 *
 *   size: the number of instructions sent
 *
 *   returns: no
 */
static void  EEPROM_SPI_SendInstruction(uint8_t *instruction, uint8_t size){
    while (EEPROM_SPI->State == HAL_SPI_STATE_RESET)
        HAL_Delay(1);
    if (HAL_SPI_Transmit(EEPROM_SPI, (uint8_t*)instruction, (uint16_t)size, 200) != HAL_OK)
        Error_Handler();
}

/*
 * Function: EEPROM_SendByte
 * ----------------------------
 *   Sends a single Byte of data to the EEPROM
 *
 *   byte: the instruction to be sent
 *
 *   returns: (uint8_t) received answer for the transfer
 */
static uint8_t EEPROM_SendByte(uint8_t byte){
    uint8_t answerByte;
    /* Loop while DR register in not empty */
    while (EEPROM_SPI->State == HAL_SPI_STATE_RESET)
        HAL_Delay(1);

    /* Send byte through the SPI peripheral */
    if (HAL_SPI_Transmit(EEPROM_SPI, &byte, 1, 200) != HAL_OK)
        Error_Handler();

    /* Wait to receive a byte */
    while (EEPROM_SPI->State == HAL_SPI_STATE_RESET)
        HAL_Delay(1);

    /* Return the byte read from the SPI bus */
    if (HAL_SPI_Receive(EEPROM_SPI, &answerByte, 1, 200) != HAL_OK)
        Error_Handler();

    return (uint8_t)answerByte;
}

/*
 * Function: sEE_WriteEnable
 * ----------------------------
 *   Enables writing to the non-volatile portion of the EEPROM
 *
 *   returns: no
 */
static void sEE_WriteEnable(void){
// Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    uint8_t command[1] = { EEPROM_WREN };
    /* Send "Write Enable" instruction */
    EEPROM_SPI_SendInstruction((uint8_t*)command, 1);

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();
}

/*
 * Function: sEE_WriteDisable
 * ----------------------------
 *   Disables writing to the non-volatile portion of the EEPROM
 *
 *   returns: no
 */
static void sEE_WriteDisable(void){
// Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    uint8_t command[1] = { EEPROM_WRDI };

    /* Send "Write Disable" instruction */
    EEPROM_SPI_SendInstruction((uint8_t*)command, 1);

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();
}

/*
 * Function: sEE_WriteStatusRegister
 * ----------------------------
 *   Writes the Status Register of the EEPROM for behavioral change
 *
 *   regval: new status
 *
 *   returns: no
 */
static void sEE_WriteStatusRegister(uint8_t regval){
    uint8_t command[2];

    command[0] = EEPROM_WRSR;
    command[1] = regval;

    // Enable the write access to the EEPROM
    sEE_WriteEnable();

    // Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    // Send "Write Status Register" instruction
    // and Regval in one packet
    EEPROM_SPI_SendInstruction((uint8_t*)command, 2);

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();

    sEE_WriteDisable();
}

/* Not implemented*/
static uint8_t sEE_ReadStatusRegister(void);
static void  EEPROM_SPI_ReadStatusByte(SPI_HandleTypeDef SPIe, uint8_t *statusByte );

/*
 * Function: EEPROM_SPI_INIT
 * ----------------------------
 * 	 Saves the pointer to the SPI peripheral used for EEPROM data transfer
 *
 *   returns: no
 */
void EEPROM_SPI_INIT(SPI_HandleTypeDef * hspi){
    EEPROM_SPI = hspi;
}

/*
 * Function: EEPROM_SPI_WritePage
 * ----------------------------
 *   Writes a full page in the EEPROM with the given data
 *
 *   pBuffer: pointer to the transmit data buffer
 *
 *   WriteAddr: the start address of the page
 *
 *   NumByteToWrite: the length of the transmit buffer
 *
 *   returns: the status of the data transmission
 */
EepromOperations EEPROM_SPI_WritePage(uint8_t* pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite){
    while (EEPROM_SPI->State != HAL_SPI_STATE_READY)
        HAL_Delay(1);

    HAL_StatusTypeDef spiTransmitStatus;

    sEE_WriteEnable();

    uint8_t header[3];

    header[0] = EEPROM_WRITE;   // Send "Write to Memory" instruction
    header[1] = WriteAddr >> 8; // Send 16-bit address
    header[2] = WriteAddr;

    // Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    EEPROM_SPI_SendInstruction((uint8_t*)header, 3);

    // Make 5 attemtps to write the data
    for (uint8_t i = 0; i < 5; i++) {
        spiTransmitStatus = HAL_SPI_Transmit(EEPROM_SPI, pBuffer, NumByteToWrite, 100);
        if (spiTransmitStatus == HAL_BUSY)
            HAL_Delay(5);
        else break;
    }

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();

    // Wait the end of EEPROM writing
    EEPROM_SPI_WaitStandbyState();

    // Disable the write access to the EEPROM
    sEE_WriteDisable();

    if (spiTransmitStatus == HAL_ERROR)
        return EEPROM_STATUS_ERROR;
    else
        return EEPROM_STATUS_COMPLETE;
}

/*
 * Function: EEPROM_SPI_WriteBuffer
 * ----------------------------
 *   Writes the contents of a buffer to the EEPROM starting with a given address
 *
 *   pBuffer: pointer to the transmit buffer
 *
 *   WriteAddr: the starting address of the write operation
 *
 *   NumByteToWrite: the effective size of the transmit buffer
 *
 *   returns: the status of the data transmission
 */
EepromOperations EEPROM_SPI_WriteBuffer(uint8_t* pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite){
    uint16_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;
    uint16_t sEE_DataNum = 0;

    EepromOperations pageWriteStatus = EEPROM_STATUS_PENDING;

    Addr = WriteAddr % EEPROM_PAGESIZE;
    count = EEPROM_PAGESIZE - Addr;
    NumOfPage =  NumByteToWrite / EEPROM_PAGESIZE;
    NumOfSingle = NumByteToWrite % EEPROM_PAGESIZE;

    if (Addr == 0) { /* WriteAddr is EEPROM_PAGESIZE aligned  */
        if (NumOfPage == 0) { /* NumByteToWrite < EEPROM_PAGESIZE */
            sEE_DataNum = NumByteToWrite;
            pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

            if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                return pageWriteStatus;

        } else { /* NumByteToWrite > EEPROM_PAGESIZE */
            while (NumOfPage--) {
                sEE_DataNum = EEPROM_PAGESIZE;
                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

                if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                    return pageWriteStatus;

                WriteAddr +=  EEPROM_PAGESIZE;
                pBuffer += EEPROM_PAGESIZE;
            }

            sEE_DataNum = NumOfSingle;
            pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

            if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                return pageWriteStatus;
        }
    } else { /* WriteAddr is not EEPROM_PAGESIZE aligned  */
        if (NumOfPage == 0) { /* NumByteToWrite < EEPROM_PAGESIZE */
            if (NumOfSingle > count) { /* (NumByteToWrite + WriteAddr) > EEPROM_PAGESIZE */
                temp = NumOfSingle - count;
                sEE_DataNum = count;
                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

                if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                    return pageWriteStatus;

                WriteAddr +=  count;
                pBuffer += count;

                sEE_DataNum = temp;
                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);
            } else {
                sEE_DataNum = NumByteToWrite;
                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);
            }

            if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                return pageWriteStatus;
        } else { /* NumByteToWrite > EEPROM_PAGESIZE */
            NumByteToWrite -= count;
            NumOfPage =  NumByteToWrite / EEPROM_PAGESIZE;
            NumOfSingle = NumByteToWrite % EEPROM_PAGESIZE;

            sEE_DataNum = count;

            pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

            if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                return pageWriteStatus;

            WriteAddr +=  count;
            pBuffer += count;

            while (NumOfPage--) {
                sEE_DataNum = EEPROM_PAGESIZE;

                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

                if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                    return pageWriteStatus;

                WriteAddr +=  EEPROM_PAGESIZE;
                pBuffer += EEPROM_PAGESIZE;
            }

            if (NumOfSingle != 0) {
                sEE_DataNum = NumOfSingle;

                pageWriteStatus = EEPROM_SPI_WritePage(pBuffer, WriteAddr, sEE_DataNum);

                if (pageWriteStatus != EEPROM_STATUS_COMPLETE)
                    return pageWriteStatus;
            }
        }
    }

    return EEPROM_STATUS_COMPLETE;
}

/*
 * Function: EEPROM_SPI_ReadBuffer
 * ----------------------------
 *   Reads a given number of bytes from the EEPROM
 *
 *   pBuffer: pointer to the receiver buffer
 *
 *   ReadAddr: the starting address of the read operation
 *
 *   NumByteToRead: the number of bytes to be read from the EEPROM
 *
 *   returns: the status of the read operation
 */
EepromOperations EEPROM_SPI_ReadBuffer(uint8_t* pBuffer, uint16_t ReadAddr, uint16_t NumByteToRead){
    while (EEPROM_SPI->State != HAL_SPI_STATE_READY)
        HAL_Delay(1);

    uint8_t header[3];

    header[0] = EEPROM_READ;    // Send "Read from Memory" instruction
    header[1] = ReadAddr >> 8;  // Send 16-bit address
    header[2] = ReadAddr;

    // Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    /* Send WriteAddr address byte to read from */
    EEPROM_SPI_SendInstruction(header, 3);

    while (HAL_SPI_Receive(EEPROM_SPI, (uint8_t*)pBuffer, NumByteToRead, 200) == HAL_BUSY)
        HAL_Delay(1);

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();

    return EEPROM_STATUS_COMPLETE;
}

/*
 * Function: EEPROM_SPI_WaitStandbyState
 * ----------------------------
 *   Probes the EEPROM for finished writing cycle flag
 *
 *   returns: 0 if the EEPROM is free
 */
uint8_t EEPROM_SPI_WaitStandbyState(void){
    uint8_t sEEstatus[1] = { 0x00 };
    uint8_t command[1] = { EEPROM_RDSR };

    // Select the EEPROM: Chip Select low
    EEPROM_CS_LOW();

    // Send "Read Status Register" instruction
    EEPROM_SPI_SendInstruction((uint8_t*)command, 1);

    // Loop as long as the memory is busy with a write cycle
    do {
        while (HAL_SPI_Receive(EEPROM_SPI, (uint8_t*)sEEstatus, 1, 200) == HAL_BUSY)
            HAL_Delay(1);
        HAL_Delay(1);
    } while ((sEEstatus[0] & EEPROM_WIP_FLAG) == SET); // Write in progress

    // Deselect the EEPROM: Chip Select high
    EEPROM_CS_HIGH();

    return 0;
}
