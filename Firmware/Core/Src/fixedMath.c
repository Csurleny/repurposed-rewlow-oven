//
// Created by Csurleny on 12/12/2021.
//

#include "fixedMath.h"
#include <math.h>

SFRAC32 temp_to_fract(uint16_t temp){
    // The temperature is considered a fixed point decimal number with 9b integer part (max 300 C)
    // we scaling factor will be 2^9 = 512
    // 1 sign bit + 9 integer bit + 22 fractional bits = 32 bits
    return (SFRAC32)(temp) << 22;
}

uint16_t fract_to_temp(SFRAC32 tempF){
    // The back conversion is done by simply removing the fractional parts
    return (uint16_t)(tempF >> 22);
}

uint16_t fract_to_duty(SFRAC32 dutyF){
    return (uint16_t)(dutyF >> 16);
}
