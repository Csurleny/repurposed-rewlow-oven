//
// Created by Csurleny on 12/4/2021.
//

#include "max6675.h"

extern SPI_HandleTypeDef hspi1;

static _Bool TCF = 0;
static uint8_t DATARX[2];

#ifdef FLOAT_RESULT
/*
 * Function: max6675_get_temp_f
 * ----------------------------
 *   Reads the temperature on the K-type thermo-couple
 *
 *   returns: the temperature in C (float)
 */
float max6675_get_temp(void){
    float temp = 0;
    //HAL_GPIO_WritePin(nSS_GPIO_Port, nSS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Receive(&hspi1, DATARX, 1, 1000);
    //HAL_GPIO_WritePin(nSS_GPIO_Port, nSS_Pin, GPIO_PIN_SET);
    TCF=(((DATARX[0]|(DATARX[1]<<8))>>2)& 0x0001);
    temp=(float)((((DATARX[0]|DATARX[1]<<8)))>>3);
    temp*=0.25f;
    return temp;
}
#else

/*
 * Function: max6675_get_temp_r
 * ----------------------------
 *   Reads the temperature on the K-type thermo-couple
 *
 *   returns: the temperature in C (raw uint16_t) as seen in the IC register 14 - 3 bits
 */
uint16_t max6675_get_temp(void){
    uint16_t temp = 0;
    HAL_GPIO_WritePin(tCS_GPIO_Port, tCS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Receive(&hspi1, DATARX, 1, 1000);
    HAL_GPIO_WritePin(tCS_GPIO_Port, tCS_Pin, GPIO_PIN_SET);

    TCF=(((DATARX[0]|(DATARX[1]<<8))>>2)& 0x0001);
    temp = (DATARX[1] << 8) | DATARX[0];
    temp = (0x0FFF & (temp >> 3));
    return temp/4;
}
#endif
