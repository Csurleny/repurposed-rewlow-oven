//
// Created by Csurleny on 12/4/2021.
//


#include "lcd.h"
#include "main.h"

#define CLEAR_DISPLAY 0x01

#define RETURN_HOME 0x02

#define ENTRY_MODE_SET 0x04
#define OPT_S	0x01					// Shift entire display to right
#define OPT_INC 0x02					// Cursor increment

#define DISPLAY_ON_OFF_CONTROL 0x08
#define OPT_D	0x04					// Turn on display
#define OPT_C	0x02					// Turn on cursor
#define OPT_B 	0x01					// Turn on cursor blink

#define CURSOR_DISPLAY_SHIFT 0x10		// Move and shift cursor
#define OPT_SC 0x08
#define OPT_RL 0x04

#define FUNCTION_SET 0x20
#define OPT_DL 0x10						// Set interface data length
#define OPT_N 0x08						// Set number of display lines
#define OPT_F 0x04						// Set alternate font
#define SETCGRAM_ADDR 0x040
#define SET_DDRAM_ADDR 0x80				// Set DDRAM address

#define D4_Pin	GPIO_PIN_0
#define D4_GPIO_Port	GPIOA
#define D5_Pin	GPIO_PIN_1
#define D5_GPIO_Port	GPIOA
#define D6_Pin	GPIO_PIN_2
#define D6_GPIO_Port	GPIOA
#define D7_Pin	GPIO_PIN_3
#define D7_GPIO_Port	GPIOA
#define RS_Pin	GPIO_PIN_4
#define RS_GPIO_Port	GPIOA
#define RW_Pin	GPIO_PIN_5
#define RW_GPIO_Port	GPIOA
#define EN_Pin	GPIO_PIN_6
#define EN_GPIO_Port	GPIOA

extern TIM_HandleTypeDef htim1;	// TIM1 for delay timing purposes

/*
 * Function: delay
 * ----------------------------
 *   Creates microsecond delays
 *
 *   us: delay value in microseconds
 *
 *   returns: no
 */
static void delay(uint16_t us){
    __HAL_TIM_SET_COUNTER(&htim1, 0);
    while(__HAL_TIM_GET_COUNTER(&htim1) < us);
}

/*
 * Function: send_to_lcd
 * ----------------------------
 *   Sends data or command to the LCD
 *
 *   data: the data byte to be sent
 *   rs: instruction or character mode
 *
 *   returns: no
 */
static void send_to_lcd(char data, int rs){
    HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, rs);

    uint32_t odr = GPIOA->ODR;
    odr |= (0x0000000F & data);
    odr &= (0xFFFFFFF0 | data);
    GPIOA->ODR = odr;

    HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, 1);
    delay(20);
    HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, 0);
    delay(20);
}

/*
 * Function: lcd_send_cmd
 * ----------------------------
 *   Sends command type data to the LCD
 *
 *   cmd: command value
 *
 *   returns: no
 */
void lcd_send_cmd(char cmd){
    char temp_to_send;

    temp_to_send = ((cmd >> 4) & 0x0F);
    send_to_lcd(temp_to_send, 0);

    temp_to_send = ((cmd) & 0x0F);
    send_to_lcd(temp_to_send, 0);
}

/*
 * Function: lcd_send_data
 * ----------------------------
 *   Sends a single character to the LCD
 *
 *   data: character value
 *
 *   returns: no
 */
void lcd_send_data(char data){
    char temp_to_send;

    temp_to_send = ((data >> 4) & 0x0F);
    send_to_lcd(temp_to_send, 1);

    temp_to_send = ((data) & 0x0F);
    send_to_lcd(temp_to_send, 1);
}

/*
 * Function: lcd_clear
 * ----------------------------
 *   Clears the LCD screen
 *
 *   returns: no
 */
void lcd_clear(void){
    lcd_send_cmd(0x01);
    HAL_Delay(2);
}

/*
 * Function: lcd_put_cur
 * ----------------------------
 *   Sets the cursor to the given location
 *
 *   row: LCD row value (0 or 1)
 *
 *   col: LCD column value (from 0 to 15)
 *
 *   returns: no
 */
void lcd_put_cur(int row, int col){
    switch(row){
        case 0:
            col |= 0x80;
            break;
        case 1:
            col |= 0xC0;
            break;
    }
    lcd_send_cmd(col);
}

/*
 * Function: lcd_init
 * ----------------------------
 *   Initializes the LCD for 4b communication,
 *   clears the display, hides the cursor
 *
 *   returns: no
 */
void lcd_init(void){
    HAL_Delay(50);
    lcd_send_cmd(0x33);
    HAL_Delay(5);
    lcd_send_cmd(0x32);
    HAL_Delay(1);
    lcd_send_cmd(FUNCTION_SET | OPT_N);
    HAL_Delay(10);
    lcd_send_cmd(CLEAR_DISPLAY);
    HAL_Delay(1);
    lcd_send_cmd(DISPLAY_ON_OFF_CONTROL | OPT_D);
}

/*
 * Function: lcd_send_string
 * ----------------------------
 *   Sends a full character array to the lcd
 *
 *   str: pointer to the source array
 *
 *   returns: no
 */
void lcd_send_string (char *str){
    while (*str) lcd_send_data (*str++);
}
