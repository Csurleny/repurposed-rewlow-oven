/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "eeprom.h"
#include "lcd.h"
#include "max6675.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfortify-source"
/* USER CODE BEGIN PTD */
typedef struct{
    char name[6];
    uint8_t T_soak;
    uint8_t dT_reflow;
    uint8_t t_soak_ramp;
    uint8_t dt_soak;
    uint8_t dt_reflow_ramp;
    uint8_t dt_reflow;
    uint8_t dt_cooling;
}reflow_profile;

typedef enum {
    INC,
    DEC,
    NONE
} encoder_states;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */
static volatile bool control_timer_flag = false;
static volatile bool display_timer_flag = false;
static volatile bool debounce_timer_flag = false;

static const float kp = 12.5e-3f; // Proportional gain 0.227
static const float ki = 2.12e-5f;   // Integral gain
static const float kd = 1.26e-3f; // derivative gain  0.025
static const float dN = 0.61e-3f;   // derivative filter coefficient 2.0
static const float kb = 25.26f;   // integral back calculation coefficient

float reference_temperature;
float measured_temperature;
float control_sat = 0.0f;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  char gui_text[16];
  uint8_t gui_page_counter = 0;
  uint8_t gui_temp_val1 = 0;
  encoder_states encoder_inc_dec = NONE;
  int16_t signed_counter[2] = {0};
  uint16_t encoder_counter = 0;
  bool over_flag = false;

    // Number of Profiles saved in EEPROM (21845 theoretical entries)
  // This is the data in the first two bytes
  uint16_t profile_num = 0;
  uint8_t eeprom_buffer[13] = {0};
  reflow_profile profile;
  const uint8_t profile_offset = 13;
  const uint8_t profile_start = 0x02;
  const char char_table[37] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                             'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1',
                             '2', '3', '4', '5', '6', '7', '8', '9', '_'};
  uint16_t profile_var = 0;
  uint8_t profile_step = 0;
  uint8_t char_pos_counter = 0;
  bool change_flag = false;

  bool reflow_start_stop_flag = false;
  bool pause_reflow_flag = false;

  uint16_t board_temp;
  uint8_t reflow_stage;
  uint16_t current_time = 0;
  uint8_t tim_cnt = 0;
  uint16_t time_array[6];
  float temperature_array[6];
  float temp_filter[3] = {0.0f, 0.0f, 0.0f};

  float error[2] = {0.0f, 0.0f};
  float integral[2] = {0.0f, 0.0f};
  float differential[2] = {0.0f, 0.0f};
  float back_calc[2] = {0.0f, 0.0f};
  float control = 0.0f;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
  EEPROM_SPI_INIT(&hspi2);
  // Read the number of stored profiles
  EEPROM_SPI_ReadBuffer(&eeprom_buffer[0], 0x00, 2);
  profile_num = (eeprom_buffer[0] << 8) | (eeprom_buffer[1] & 0x00FF);
  sprintf(profile.name, "______");

  HAL_TIM_Base_Start_IT(&htim1);
  lcd_init();
  sprintf(&gui_text[0], "StartingFurnace ");
  lcd_put_cur(0, 0);
  lcd_send_string(&gui_text[0]);
  lcd_put_cur(1, 0);
  lcd_send_string(&gui_text[8]);
  HAL_Delay(1000);

  HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
      // check for control event -- highest priority
      if(control_timer_flag){
          // temperature measurements
          temp_filter[0] = temp_filter[1];
          temp_filter[1] = temp_filter[2];
          temp_filter[2] = (float)(max6675_get_temp());

          // Median filter to remove impulse noise
          if(temp_filter[0] <= temp_filter[1] && temp_filter[0] <= temp_filter[2])
              measured_temperature = (temp_filter[1] <= temp_filter[2]) ? temp_filter[1] : temp_filter[2];
          else if(temp_filter[1] <= temp_filter[0] && temp_filter[1] <= temp_filter[2])
              measured_temperature = (temp_filter[0] <= temp_filter[2]) ? temp_filter[0] : temp_filter[2];
          else
              measured_temperature = (temp_filter[0] <= temp_filter[1]) ? temp_filter[1] : temp_filter[0];

          // Put the control algorithm here
          if(reflow_start_stop_flag){
              // The tracking control is on
              if(!pause_reflow_flag){
                  // Normal operation
                  ++current_time;

                  // Calculate the piece-wise linear temperature reference
                  if(current_time < time_array[1]){
                      // Ramp up to soaking temperature
                      reflow_stage = 0;
                  }

                  if(current_time > time_array[1] && current_time < time_array[2]){
                      // Soaking on constant temperature
                      reflow_stage = 1;
                  }

                  if(current_time > time_array[2] && current_time < time_array[3]){
                      // Ramp up to reflow temperature
                      reflow_stage = 2;
                  }

                  if(current_time > time_array[3] && current_time < time_array[4]){
                      // Reflow on constant temperature
                      reflow_stage = 3;
                  }

                  if(current_time > time_array[4] && current_time < time_array[5]) {
                      // Cooling ramp temperature
                      reflow_stage = 4;
                  }

                  float a = (temperature_array[reflow_stage+1] - temperature_array[reflow_stage]) /
                          (float)(time_array[reflow_stage+1] - time_array[reflow_stage]);
                  float b = (temperature_array[reflow_stage]*(float)time_array[reflow_stage+1] -
                          temperature_array[reflow_stage+1]* (float)time_array[reflow_stage]) /
                            (float)(time_array[reflow_stage+1] - time_array[reflow_stage]);
                  reference_temperature = a * (float)current_time + b;

                  // Error
                  error[0]= reference_temperature - measured_temperature;
                  //error[0]= 200.0f - measured_temperature;
                  // Integral term with conditional clamping
                  back_calc[0] = error[0] - kb*(control - control_sat);
                  integral[0] = integral[1] + 0.5f*(back_calc[0] + back_calc[1]);
                  back_calc[1] = back_calc[0];
                  integral[1] = integral[0];
                  // derivative term
                  differential[0] = ((2.0f*dN - 1.0f) /(2.0f*dN + 1))*differential[0] +
                          (2.0f/(2.0f*dN + 1)) *(error[0] - error[1]);
                  differential[1] = differential[0];
                  // output saturation
                  control = kp*error[0] + ki*integral[0] + kd*differential[0];
                  control_sat = control;
                  if(control < 0.0f)
                      control_sat = 0.0f;
                  if(control > 1.0f)    // max 80% duty
                      control_sat = 1.0f;
                  error[1] = error[0];

                  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, (uint16_t)(control_sat * 50000.0f));

                  //Bang-Bang control
                  /*if(out_reference - measured_temperature > 0)
                      __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, 50000); // 100%
                  else
                      __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, 0); // 0%*/
                  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);

                  if(current_time == time_array[5]){
                      gui_temp_val1 = 6;
                      HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_ALL);
                      TIM2->PSC = 164 - 1;
                      TIM2->ARR = 1000-1;
                      TIM2->CCR3 = 0;
                      TIM2->CCR4 = 500-1;
                  }
              }else{
                  // Keep a constant temperature
              }
          }else{
              current_time = 0;
          }
          // End of control algorithm
          control_timer_flag = false;
#ifdef USE_GLOBAL_DEBUG_VAR
          out_reflow_start = 0;
#endif
      } else{
          // check for display refresh event -- lowest priority
          if(display_timer_flag) {
              // Put the GUI FSM responses to the button event
              // get the encoder value
              encoder_counter = __HAL_TIM_GET_COUNTER(&htim4);
              signed_counter[1] = signed_counter[0];
              signed_counter[0] = (int16_t)(encoder_counter);
              if(signed_counter[0] - signed_counter[1] > 0)
                  encoder_inc_dec = INC;
              if(signed_counter[0] - signed_counter[1] < 0)
                  encoder_inc_dec = DEC;
              if(signed_counter[0] - signed_counter[1] == 0)
                  encoder_inc_dec = NONE;
              //FSM
              switch(gui_page_counter){
                  case 0:

                      // Select Load / Create mode
                      sprintf(&gui_text[0], " Load    Create ");
                      if(gui_temp_val1 == 0){
                          gui_text[0] = '>';
                          gui_text[8] = ' ';
                      }
                      if(gui_temp_val1 == 1){
                          gui_text[0] = ' ';
                          gui_text[8] = '>';
                      }

                      if(debounce_timer_flag){
                          if(gui_temp_val1 == 0)
                              gui_page_counter = 1;
                          if(gui_temp_val1 == 1)
                              gui_page_counter = 2;
                          gui_temp_val1 = 0;
                          debounce_timer_flag = false;
                      }

                      if(encoder_inc_dec == INC)
                          gui_temp_val1 = 0;
                      if(encoder_inc_dec == DEC)
                          gui_temp_val1 = 1;

                      lcd_put_cur(0, 0);
                      lcd_send_string(&gui_text[0]);
                      lcd_put_cur(1, 0);
                      lcd_send_string(&gui_text[8]);

                      break;
                  case 1:
                      // Select a profile to load
                      if(!change_flag) {
                      EEPROM_SPI_ReadBuffer((uint8_t*)(&profile),
                                            profile_start + gui_temp_val1, 13);
                      time_array[0] = 0;
                      time_array[1] = profile.t_soak_ramp;
                      time_array[2] = time_array[1] + profile.dt_soak;
                      time_array[3] = time_array[2] + profile.dt_reflow_ramp;
                      time_array[4] = time_array[3] + profile.dt_reflow;
                      time_array[5] = time_array[4] + profile.dt_cooling;

                      temperature_array[0] = measured_temperature;
                      temperature_array[1] = profile.T_soak;
                      temperature_array[2] = temperature_array[1];
                      temperature_array[3] = temperature_array[1] + (float)profile.dT_reflow;
                      temperature_array[4] = temperature_array[3];
                      temperature_array[5] = temperature_array[0];

                      }
                      sprintf(&gui_text[0], "  Load   %s ", profile.name);

                      if(encoder_inc_dec == INC && gui_temp_val1 < profile_num) {
                          ++gui_temp_val1;
                      }
                      if(encoder_inc_dec == DEC && gui_temp_val1 > 0) {
                          --gui_temp_val1;
                      }

                      if(encoder_inc_dec == NONE)
                          change_flag = true;
                      else
                          change_flag = false;

                      lcd_put_cur(0, 0);
                      lcd_send_string(&gui_text[0]);
                      lcd_put_cur(1, 0);
                      lcd_send_string(&gui_text[8]);

                      if(debounce_timer_flag){
                          gui_page_counter = 3;
                          reflow_start_stop_flag = true;
                          HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_ALL);
                          TIM2->PSC = 360 - 1;
                          TIM2->ARR = 50000-1;
                          TIM2->CCR3 = 0;
                          HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
                          debounce_timer_flag = false;
                      }

                      break;
                  case 2:
                      // Profile creation soak temp
                      if(profile_step == 0){
                          // soak temperature
                          sprintf(&gui_text[0], "SoakTemp %3d  C ", profile_var);
                          gui_text[13] = 0b11011111;

                          if(debounce_timer_flag){
                              profile.T_soak = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 1){
                          sprintf(&gui_text[0], "ReflTemp  %3d  C ", profile_var);
                          gui_text[13] = 0b11011111;

                          if(debounce_timer_flag){
                              profile.dT_reflow = profile_var - profile.T_soak;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 2){
                          sprintf(&gui_text[0], "sRmpTime%3d  s  ", profile_var);

                          if(debounce_timer_flag){
                              profile.t_soak_ramp = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 3){
                          sprintf(&gui_text[0],"SoakTime %3d  s ", profile_var);

                          if(debounce_timer_flag){
                              profile.dt_soak = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 4){
                          sprintf(&gui_text[0], "rRmpTime  %3d  s ", profile_var);

                          if(debounce_timer_flag){
                              profile.dt_reflow_ramp = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 5){
                          sprintf(&gui_text[0], "RflTime  %3d  s ", profile_var);

                          if(debounce_timer_flag){
                              profile.dt_reflow = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 6){
                          sprintf(&gui_text[0], "CoolTime %3d  s ", profile_var);

                          if(debounce_timer_flag){
                              profile.dt_cooling = profile_var;
                              profile_var = 0;
                              profile_step++;
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 7){
                          profile.name[char_pos_counter] = char_table[profile_var % 37];
                          sprintf(&gui_text[0], "Name:    %6s ", profile.name);

                          if(debounce_timer_flag){
                              char_pos_counter++;
                              if(char_pos_counter == 6){
                                  profile_var = 0;
                                  profile_step++;
                              }
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 8){
                          sprintf(&gui_text[0], " Save?   Y    N ");
                          if(profile_var % 2 == 0){
                              gui_text[8] = '>';
                              gui_text[13] = ' ';
                          }else{
                              gui_text[8] = ' ';
                              gui_text[13] = '>';
                          }
                          if(debounce_timer_flag){
                              if(profile_var % 2 == 0) {
                                  profile_step++;
                                  profile_var = 1;
                              }else{
                                  gui_page_counter = 0;
                                  gui_temp_val1 = 0;
                              }
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 9){
                          sprintf(&gui_text[0], "Location %3d    ", profile_var);

                          if(debounce_timer_flag){
                              if(profile_var > profile_num)
                                  profile_step = 10;
                              else {
                                  profile_step = 11;
                                  over_flag = false;
                              }
                              profile_var = 0;
                          }
                      }

                      if(profile_step == 10){
                          sprintf(&gui_text[0], "OvrWrt?  Y    N ");
                          if(profile_var % 2 == 0){
                              gui_text[8] = '>';
                              gui_text[13] = ' ';
                          }else{
                              gui_text[8] = ' ';
                              gui_text[13] = '>';
                          }
                          if(debounce_timer_flag){
                              if(profile_var % 2 == 0) {
                                  profile_step = 11;
                                  profile_var = 1;
                                  over_flag = true;
                              }else{
                                  profile_step = 9;
                              }
                              debounce_timer_flag = false;
                          }
                      }

                      if(profile_step == 11){
                          if(over_flag == false){
                              ++profile_num;
                              eeprom_buffer[0] = profile_num >> 8;
                              eeprom_buffer[1] = profile_num & 0xFF;
                              EEPROM_SPI_WriteBuffer(&eeprom_buffer[0], 0x00, 2);
                          }

                          EEPROM_SPI_WriteBuffer((uint8_t*)(&profile),
                                                 profile_start + (profile_num-1)*profile_offset, 13);


                          gui_page_counter = 0;
                          gui_temp_val1 = 0;
                      }

                      if(encoder_inc_dec == DEC && profile_var <= 500){
                          ++profile_var;
                      }
                      if (encoder_inc_dec == INC && profile_var > 0) {
                          --profile_var;
                      }



                      lcd_put_cur(0, 0);
                      lcd_send_string(&gui_text[0]);
                      lcd_put_cur(1, 0);
                      lcd_send_string(&gui_text[8]);

                      break;
                  case 3:
                      // Start the loaded profile tracking
                      if(gui_temp_val1 == 0){
                          // Show reflow stage
                          sprintf(&gui_text[0], "Tracking");
                          if(reflow_stage == 0){
                              sprintf(&gui_text[8], "Pre-Heat");
                          }
                          if(reflow_stage == 1){
                              sprintf(&gui_text[8], "  Soak  ");
                          }
                          if(reflow_stage == 2){
                              sprintf(&gui_text[8], "Ramp-Up ");
                          }
                          if(reflow_stage == 3){
                              sprintf(&gui_text[8], "Re-Flow ");
                          }
                          if(reflow_stage == 4){
                              sprintf(&gui_text[8], "Cooling ");
                          }
                          if(debounce_timer_flag){
                              ++gui_temp_val1;
                              debounce_timer_flag = false;
                          }
                      }
                      if(gui_temp_val1 == 1){
                          // Show temperatures

                          sprintf(&gui_text[0], "Tm:%3u CTr:%3u C", (uint16_t)(measured_temperature),
                                   (uint16_t)(reference_temperature));
                          if(debounce_timer_flag){
                              ++gui_temp_val1;
                              debounce_timer_flag = false;
                          }
                      }
                      if(gui_temp_val1 == 2){
                          // Show ETA
                          uint8_t minutes = (time_array[4] - current_time)/60;
                          uint8_t seconds = (time_array[4] - current_time) % 60;
                          sprintf(&gui_text[0], "ETA:    -%2d:%2d s", minutes, seconds);
                          if(debounce_timer_flag){
                              ++gui_temp_val1;
                              debounce_timer_flag = false;
                          }
                      }
                      if(gui_temp_val1 == 3){
                          // Pause reflow
                          sprintf(&gui_text[0], "Pause?   Y    N ");
                          if(profile_var % 2 == 0){
                              gui_text[8] = '>';
                              gui_text[13] = ' ';
                          }else{
                              gui_text[8] = ' ';
                              gui_text[13] = '>';
                          }
                          if(debounce_timer_flag){
                              if(profile_var % 2 == 0) {
                                  gui_temp_val1 = 4;
                                  pause_reflow_flag = true;
                              }else{
                                  gui_temp_val1 = 5;
                              }
                              profile_var = 0;
                              debounce_timer_flag = false;
                          }
                      }
                      if(gui_temp_val1 == 4){
                          // Resume Reflow
                          sprintf(&gui_text[0], "Resume?  Y    N ");
                          if(profile_var % 2 == 0){
                              gui_text[8] = '>';
                              gui_text[13] = ' ';
                          }else{
                              gui_text[8] = ' ';
                              gui_text[13] = '>';
                          }
                          if(debounce_timer_flag){
                              if(profile_var % 2 == 0) {
                                  gui_temp_val1 = 3;
                                  profile_var = 0;
                                  pause_reflow_flag = false;
                              }else{
                                  profile_var = 0;
                                  gui_temp_val1 = 0;
                              }
                              debounce_timer_flag = false;
                          }
                      }
                      if(gui_temp_val1 == 5){
                          // Stop Reflow
                          sprintf(&gui_text[0], "Stop?    Y    N ");
                          if(profile_var % 2 == 0){
                              gui_text[8] = '>';
                              gui_text[13] = ' ';
                          }else{
                              gui_text[8] = ' ';
                              gui_text[13] = '>';
                          }
                          if(debounce_timer_flag){
                              if(profile_var % 2 == 0) {
                                  gui_temp_val1 = 0;
                                  profile_var = 0;
                                  reflow_start_stop_flag = false; // stop the reflow process
                              }else{
                                  profile_var = 0;
                                  gui_temp_val1 = 0;
                              }
                              debounce_timer_flag = false;
                          }
                      }

                      if(gui_temp_val1 == 6){
                          // the profile is finished
                          sprintf(&gui_text[0], "Finished~~~~~~~~");
                          tim_cnt = (tim_cnt+1) % 100;
                          if(tim_cnt == 0)
                              HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
                          if(tim_cnt == 50)
                              HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_4);
                          if(debounce_timer_flag){
                              HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_4);
                              TIM2->PSC = 360-1;
                              TIM2->ARR = 50000-1;
                              reflow_start_stop_flag = false;
                              gui_temp_val1 = 0;
                              profile_var = 0;
                              gui_page_counter = 0;
                              debounce_timer_flag = false;
                          }
                      }

                      if(encoder_inc_dec == DEC && profile_var <= 500){
                          ++profile_var;
                      }
                      if (encoder_inc_dec == INC && profile_var > 0) {
                          --profile_var;
                      }



                      lcd_put_cur(0, 0);
                      lcd_send_string(&gui_text[0]);
                      lcd_put_cur(1, 0);
                      lcd_send_string(&gui_text[8]);


                      break;
                  default:
                      break;
              }
              // End of button event driven FSM
              display_timer_flag = false;
          }
      }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_13CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES_RXONLY;
  hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 360-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 50000-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 360-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(rCS_GPIO_Port, rCS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(tCS_GPIO_Port, tCS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3
                           PA4 PA5 PA6 tCS_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|tCS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA7 PA8 PA9 PA10
                           PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB2 PB5 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_5|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : rCS_Pin */
  GPIO_InitStruct.Pin = rCS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(rCS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : BTN_Pin */
  GPIO_InitStruct.Pin = BTN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
// Buton event detection
volatile bool debounce_state_flag = true;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_PIN){
    if(GPIO_PIN == BTN_Pin && debounce_state_flag == true){
        // Debounce the button using TIM3
        // no new debounce event is started before timer event
        debounce_state_flag = false;
        HAL_TIM_Base_Start_IT(&htim3);
    }else{
        asm("NOP");
    }
}

// Timer interrupt events
static uint16_t counter_1s = 0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim){
    if(htim == &htim1){
        // 1ms timer event
        ++counter_1s;
        if(counter_1s == 1000){
            // 1s timing flag for the control
            control_timer_flag = true;
            counter_1s = 0;
        }
        if(counter_1s % 20 == 0){
            // 20ms timing flag for the display refresh
            display_timer_flag = true;
        }

    }
    if(htim == &htim3){
        // Button debounce timer event
        if(HAL_GPIO_ReadPin(BTN_GPIO_Port, BTN_Pin) == GPIO_PIN_SET){
            debounce_state_flag = true;
            debounce_timer_flag = true;
            HAL_TIM_Base_Stop_IT(&htim3);
        }
    }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */


#pragma clang diagnostic pop
