
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "F:/Programming/CLion/Projects/FurnaceControl/startup/startup_stm32f103xb.s" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/startup/startup_stm32f103xb.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "STM32F103xB"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32F1xx/Include"
  "../Drivers/CMSIS/Include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/eeprom.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/eeprom.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/fixedMath.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/fixedMath.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/lcd.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/lcd.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/main.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/main.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/max6675.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/max6675.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/stm32f1xx_hal_msp.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/stm32f1xx_hal_msp.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/stm32f1xx_it.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/stm32f1xx_it.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/syscalls.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/syscalls.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Core/Src/system_stm32f1xx.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Core/Src/system_stm32f1xx.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc_ex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc_ex.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c.obj"
  "F:/Programming/CLion/Projects/FurnaceControl/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c" "F:/Programming/CLion/Projects/FurnaceControl/cmake-build-debug/CMakeFiles/FurnaceControl.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STM32F103xB"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32F1xx/Include"
  "../Drivers/CMSIS/Include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
