# Repurposed Rewlow Oven

How to make a reflow oven for PCB manufacturing out of a conventional cheap electric oven.


## Description
This repo serves as accompainement for the project described on <a href="https://fehera.wixsite.com/automatixaron/diy-reflow-oven"> my page </a>.

Structure:
   - Hardware: Contains the necessary schematics, PCB design files, and bills of materials:
      - EMI filter: passive LC EMI filter to reduce the injected noise due to the power switching.
      - Solid state relay using optoisolated TRIAC 
      - Control board based on the STM32F103C8T6 microcontroller 
   - Firmware for the STM MCU
   - Software to debug the MCU and to tune the controller

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
The contents of this repo are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

